﻿using ExhibitionApp.Data.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Abstract
{
    public interface IFeedBackService
    {
        FeedBack AddFeedBack(FeedBack model);
        List<FeedBack> GetByCompanyId(string compId);
    }
}
