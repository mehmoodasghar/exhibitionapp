﻿using ExhibitionApp.Data.DataModels;
using ExhibitionApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Abstract
{
    public interface ICompanyService
    {
        Company Login(LoginModel model);
        Company Add(Company company);
        void Delete(string companyId);

        List<Company> GetAll();
        Company GetById(string companyId);
        Company UpdateCompany(Company company);

        bool UpdatePassword(string id,string oldp, string newp);
    }
}
