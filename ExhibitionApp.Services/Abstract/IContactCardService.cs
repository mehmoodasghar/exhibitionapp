﻿using ExhibitionApp.Data.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Abstract
{
    public interface IContactCardService
    {
        void AddCard(ContactCard card);
        List<ContactCard> GetCardsByCompany(string compId);
    }
}
