﻿using ExhibitionApp.Data.DataModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Abstract
{
    public interface IStallService : IDisposable
    {
        void Create();
        List<Stall> GetAllStalls();
        List<Stall> GetBookedStalls();
        List<Stall> GetFreeStalls();

        List<Stall> GetStallByCompanyId(string companyId);
        Stall GetStallByNumber(int num);
        void AssignStallTocompany(string compId, int stallNum);
        void AssignStallTocompany(string compId, string stallId);

        void ReleaseStall(string stallId);
        void ReleaseStall(int num);
        void UpdateStall(Stall stall);

    }
}
