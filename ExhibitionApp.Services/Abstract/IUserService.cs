﻿using ExhibitionApp.Data.DataModels;
using ExhibitionApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Abstract
{
    public interface IUserService : IDisposable
    {
        User AddUser(User user);
        User UpdateUser(User user);


        User Login(LoginModel loginModel);

        bool UpdatePassword(string id,string oldp, string newp);
    }
}
