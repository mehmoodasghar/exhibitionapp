﻿using ExhibitionApp.Common;
using ExhibitionApp.Data.DataModels;
using ExhibitionApp.Services.Abstract;
using ExhibitionApp.ViewModels;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Implementation
{
    public class CompanyService : ICompanyService
    {
        IMongoCollection<Company> _companies;

        public CompanyService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("DatabaseUrl"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _companies = database.GetCollection<Company>("Companies");
        }

        public Company Add(Company company)
        {
            var comp = _companies.Find(x => x.Email == company.Email).FirstOrDefault();
            if (comp == null)
            {
                company.PassWord = PSecurity.Encrypy(company.PassWord);
                company.Email = company.Email.ToLower();
                _companies.InsertOne(company);
                return company;
            }
            return null;
        }

        public void Delete(string companyId)
        {
            var company = _companies.Find(x => x.Id == companyId).FirstOrDefault();
            if(company!=null)
            {
                company.IsActive = false;
                var filter = Builders<Company>.Filter.Eq(x => x.Id , companyId);
                var result = _companies.ReplaceOne(filter, company);
            }
        }

        public List<Company> GetAll()
        {
            return _companies.Find(x => x.IsActive == true).ToList();
        }

        public Company GetById(string companyId)
        {
            return _companies.Find(x => x.Id == companyId).FirstOrDefault();
        }

        public Company Login(LoginModel model)
        {
            var company = _companies.Find(x => x.UserName == model.UserName&&x.IsActive==true).FirstOrDefault();
            if(company!=null)
            {
                if(model.Password==PSecurity.Decrypt(company.PassWord))
                {
                    return company;
                }
                else
                {
                    return null;
                }
            }
            return company;
        }

        public Company UpdateCompany(Company company)
        {
            var comp = _companies.Find(x => x.Id == company.Id).FirstOrDefault();
            if(comp!=null)
            {
                comp.Address = company.Address;
                comp.ContactNo = company.ContactNo;
                comp.ContactPerson = company.ContactPerson;
                comp.CreatedOn = company.CreatedOn;
                comp.LogoPath = company.LogoPath;
                comp.PassWord = PSecurity.Encrypy(company.PassWord);
                comp.Title = company.Title;
                comp.UpdatedOn = DateTime.Now;
                comp.UserName = company.UserName;
                var filter = Builders<Company>.Filter.Eq(x => x.Id, company.Id);
                var result = _companies.ReplaceOne(filter, comp);
            }
            return comp;
        }

        public bool UpdatePassword(string id,string oldp, string newp)
        {
            var comp = _companies.Find(x => x.Id == id).FirstOrDefault();
            if(comp!=null)
            {
                var password = PSecurity.Decrypt(comp.PassWord);
                if(password==oldp)
                {
                    comp.PassWord = PSecurity.Encrypy(newp);
                    var filter = Builders<Company>.Filter.Eq(x => x.Id, id);
                    var result = _companies.ReplaceOne(filter, comp);
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}
