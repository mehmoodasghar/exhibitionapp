﻿using ExhibitionApp.Data.DataModels;
using ExhibitionApp.Services.Abstract;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Implementation
{
    public class ContactCardService : IContactCardService
    {
        IMongoCollection<ContactCard> _cards;

        public ContactCardService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("DatabaseUrl"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _cards = database.GetCollection<ContactCard>("ContactCards");
        }

        public void AddCard(ContactCard card)
        {
            _cards.InsertOne(card);
        }

        public List<ContactCard> GetCardsByCompany(string compId)
        {
            return _cards.Find(x => x.CompanyId == compId).ToList();
        }
    }
}
