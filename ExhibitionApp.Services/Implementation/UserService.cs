﻿using ExhibitionApp.Common;
using ExhibitionApp.Data.DataModels;
using ExhibitionApp.Services.Abstract;
using ExhibitionApp.ViewModels;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Implementation
{
    public class UserService : IUserService
    {
        IMongoCollection<User> _users;

        public UserService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("DatabaseUrl"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _users = database.GetCollection<User>("Users");
        }

        public User AddUser(User user)
        {
            if (_users.Find(x => true).ToList().Count == 0)
            {
                user.Password = PSecurity.Encrypy(user.Password);
                _users.InsertOne(user);
            }
            return user;
        }

        public User UpdateUser(User user)
        {
            var u = _users.Find(x => x.Id == user.Id).FirstOrDefault();
            if(u!=null)
            {
                u.Email = user.Email;
                u.Name = user.Name;
                u.Password = PSecurity.Encrypy(user.Password);
                u.UpdatedOn = DateTime.Now;
                u.UserName = user.UserName;
                var filter = Builders<User>.Filter.Eq(x => x.Id , user.Id);
                var result = _users.ReplaceOne(filter, u);
            }
            return u;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public void Dispose(bool v)
        {
            if (v)
            {
                v = false;
            }
            else
            {
                v = true;
            }
        }

        public User Login(LoginModel loginModel)
        {
            loginModel.UserName = loginModel.UserName.ToLower();
            var user = _users.Find(x => x.UserName == loginModel.UserName).FirstOrDefault();
            if(user!=null)
            {
                if(loginModel.Password==PSecurity.Decrypt(user.Password))
                {
                    return user;
                }
                else
                {
                    return null;
                }
            }
            return user;
        }

        public bool UpdatePassword(string id, string oldp, string newp)
        {
            var user = _users.Find(x => x.Id == id).FirstOrDefault();
            if (user != null)
            {
                var password = PSecurity.Decrypt(user.Password);
                if (password == oldp)
                {
                    user.Password = PSecurity.Encrypy(newp);
                    var filter = Builders<User>.Filter.Eq(x => x.Id, id);
                    var result = _users.ReplaceOne(filter, user);
                    return true;
                }
                return false;
            }
            return false;
        }
    }
}
