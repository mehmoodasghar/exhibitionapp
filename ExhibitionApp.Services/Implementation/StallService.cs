﻿using ExhibitionApp.Data.DataModels;
using ExhibitionApp.Services.Abstract;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Implementation
{
    public class StallService : IStallService
    {
        IMongoCollection<Stall> _stalls;
        IMongoCollection<Company> _company;
        public StallService (IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("DatabaseUrl"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _stalls = database.GetCollection<Stall>("Stalls");
            _company = database.GetCollection<Company>("Companies");
        }

        public void AssignStallTocompany(string compId,int stallNum)
        {
            var stall = _stalls.Find(x => x.StallNo == stallNum).FirstOrDefault();
            if(stall!=null)
            {
                stall.CompanyId = compId;
                stall.CompanyTitle = _company.Find(x => x.Id == compId).FirstOrDefault().Title;
                stall.IsBooked = true;

                var filter = Builders<Stall>.Filter.Eq(x => x.StallNo, stallNum);
                var result = _stalls.ReplaceOne(filter, stall);
            }
        }


        public void Create()
        {
            for(int i =1; i<=8;i++)
            {
                Stall stall = new Stall();
                stall.CreatedOn = DateTime.Now;
                stall.IsBooked = false;
                stall.StallNo = i;
                stall.UpdatedOn = DateTime.Now;
                _stalls.InsertOne(stall);
            }
        }

        public List<Stall> GetAllStalls()
        {
            return _stalls.Find(x => true).ToList();
        }

        public List<Stall> GetBookedStalls()
        {
            return _stalls.Find(x => x.IsBooked == true).ToList();
        }

        public List<Stall> GetFreeStalls()
        {
            return _stalls.Find(x => x.IsBooked == false).ToList();
        }

        public List<Stall> GetStallByCompanyId(string companyId)
        {
            return _stalls.Find(x => x.IsBooked == true&&x.CompanyId==companyId).ToList();
        }

        public Stall GetStallByNumber(int num)
        {
            return _stalls.Find(x => x.StallNo==num).FirstOrDefault();
        }

        public void ReleaseStall(string stallId)
        {
            var stall = _stalls.Find(x => x.Id == stallId).FirstOrDefault();
            if (stall != null)
            {
                stall.CompanyId = null;
                stall.CompanyTitle = null ;
                stall.IsBooked = false;

                var filter = Builders<Stall>.Filter.Eq(x => x.Id, stallId);
                var result = _stalls.ReplaceOne(filter, stall);
            }
        }

        public void ReleaseStall(int num)
        {
            var stall = _stalls.Find(x => x.StallNo == num).FirstOrDefault();
            if (stall != null)
            {
                stall.CompanyId = null;
                stall.CompanyTitle = null;
                stall.IsBooked = false;

                var filter = Builders<Stall>.Filter.Eq(x => x.StallNo, num);
                var result = _stalls.ReplaceOne(filter, stall);
            }
        }

        public void UpdateStall(Stall stall)
        {
            var s = _stalls.Find(x => x.Id == stall.Id).FirstOrDefault();
            if (s != null)
            {
                if (stall.Logo != null)
                    s.Logo = stall.Logo;
                if(stall.Image1!=null)
                    s.Image1 = stall.Image1;
                if(stall.Image2!=null)
                    s.Image2 = stall.Image2;
                if(stall.Image3!=null)
                    s.Image3 = stall.Image3;
                if(stall.Image4!=null)
                    s.Image4 = stall.Image4;
                if(stall.Pdf!=null)
                    s.Pdf = stall.Pdf;
                if(stall.YoutubeLink!=null)
                    s.YoutubeLink = stall.YoutubeLink;
                if(stall.Message!=null)
                    s.Message = stall.Message;

                var filter = Builders<Stall>.Filter.Eq(x => x.Id, stall.Id);
                var result = _stalls.ReplaceOne(filter, s);
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }

        public void Dispose(bool v)
        {
            if (v)
            {
                v = false;
            }
            else
            {
                v = true;
            }
        }

        public void AssignStallTocompany(string compId, string stallId)
        {
            var stall = _stalls.Find(x => x.Id==stallId).FirstOrDefault();
            if (stall != null)
            {
                stall.CompanyId = compId;
                stall.CompanyTitle = _company.Find(x => x.Id == compId).FirstOrDefault().Title;
                stall.IsBooked = true;

                var filter = Builders<Stall>.Filter.Eq(x => x.Id, stallId);
                var result = _stalls.ReplaceOne(filter, stall);
            }
        }
    }
}
