﻿using ExhibitionApp.Data.DataModels;
using ExhibitionApp.Services.Abstract;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Services.Implementation
{
    public class FeedBackService : IFeedBackService
    {
        IMongoCollection<FeedBack> _feedBack;

        public FeedBackService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("DatabaseUrl"));
            var database = client.GetDatabase(config.GetConnectionString("Database"));
            _feedBack = database.GetCollection<FeedBack>("FeedBack");
        }

        public FeedBack AddFeedBack(FeedBack model)
        {
            _feedBack.InsertOne(model);
            return model;
        }

        public List<FeedBack> GetByCompanyId(string compId)
        {
            return _feedBack.Find(x => x.CompanyId == compId).ToList();
        }
    }
}
