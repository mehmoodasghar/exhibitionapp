﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.ViewModels
{
    public class ContactCardModal
    {
        public string CompanyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
    }
}
