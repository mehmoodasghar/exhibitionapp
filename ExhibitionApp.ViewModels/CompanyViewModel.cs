﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ExhibitionApp.ViewModels
{
    public class CompanyViewModel
    {
        public string Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Address { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string ContactNo { get; set; }
        [DisplayName("Contact Person")]
        public string ContactPerson { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
