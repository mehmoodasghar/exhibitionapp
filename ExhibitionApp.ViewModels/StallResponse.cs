﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.ViewModels
{
    public class StallResponse
    {
        public string StallId { get; set; }
        public string Logourl { get; set; }
        public string pdfUrl { get; set; }
        public string Image1Url { get; set; }
        public string Image2Url { get; set; }
        public string Image3Url { get; set; }
        public string Image4Url { get; set; }
        public string YoutubeLink { get; set; }
    }
}
