﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Data.DataModels
{
    public class ContactCard : BaseEntity
    {
        public string CompanyId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
    }
}
