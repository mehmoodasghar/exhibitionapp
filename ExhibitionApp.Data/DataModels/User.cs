﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Data.DataModels
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string Role { get; set; }


    }
}
