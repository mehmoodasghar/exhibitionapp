﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Data.DataModels
{
    public class Stall : BaseEntity
    {
        public int StallNo { get; set; }
        public string Logo { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Pdf { get; set; }
        public string YoutubeLink { get; set; }
        public string Message { get; set; }
        public bool IsBooked { get; set; }

        public string CompanyId { get; set; }
        public string CompanyTitle { get; set; }
    }
}
