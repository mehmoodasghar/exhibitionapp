﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExhibitionApp.Data.DataModels
{
    public class FeedBack : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }

        public string StallNo { get; set; }
        public string CompanyId { get; set; }
    }
}
