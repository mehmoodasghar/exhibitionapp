﻿using ExhibitionApp.Data.DataModels;
using ExhibitionApp.Services.Abstract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ExhibitionApp.Controllers
{
    public class StallController : Controller
    {
        IStallService _stallService;
        ICompanyService _companyService;
        IHttpContextAccessor _HttpContextAccessor;
        IWebHostEnvironment _webHostEnvironment;
        public StallController(IStallService stallService,ICompanyService companyService,IHttpContextAccessor httpContextAccessor,IWebHostEnvironment webHostEnvironment)
        {
            _stallService = stallService;
            _companyService = companyService;
            _HttpContextAccessor = httpContextAccessor;
            _webHostEnvironment = webHostEnvironment;
        }
        public IActionResult Index()
        {
            List<Stall> stalls;
            ViewBag.companies = _companyService.GetAll();
            if (HttpContext.Session.GetString("Role")=="Admin")
            {
                
                stalls = _stallService.GetAllStalls();
            }
            else if(HttpContext.Session.GetString("Role")=="Company")
            {
                var companyId = HttpContext.Session.GetString("Id");
                stalls = _stallService.GetStallByCompanyId(companyId);
            }
            else
            {
                stalls = _stallService.GetBookedStalls();
            }
            return View(stalls);
        }

        public IActionResult AssignToStall(string stallid, string companyid)
        {
            _stallService.AssignStallTocompany(companyid, stallid);
            return Json(new { Success = true });
        }

        public IActionResult ClearStall(string stallid)
        {
            _stallService.ReleaseStall(stallid);
            return Json(new { Success = true });
        }
        [HttpPost]
        public IActionResult UpdateStall(IFormFile logo,[Required]IFormFile image1,[Required] IFormFile image2,[Required] IFormFile image3, [Required] IFormFile image4, [Required] IFormFile pdf,[Required] string youtubelink,string message)
        {
            var stallid = _HttpContextAccessor.HttpContext.Session.GetString("Stall");
            var stall = new Stall();
            stall.Id = stallid;
            stall.Logo = UploadFile(logo,-1);
            stall.Image1 = UploadFile(image1,1);
            stall.Image2 = UploadFile(image2,2);
            stall.Image3 = UploadFile(image3,3);
            stall.Image4 = UploadFile(image4,4);
            stall.Pdf = UploadFile(pdf, 0);
            stall.Message = message;
            stall.YoutubeLink = youtubelink;
            _stallService.UpdateStall(stall);
            return RedirectToAction("index", "Stall");
        }

        private string UploadFile(IFormFile image,int index)
        {
            string uniqfilename = null;
            if(image!=null)
            {
                string uploadfolder = Path.Combine(_webHostEnvironment.WebRootPath, "uploads");
                if (index > 0)
                {
                   
                    uniqfilename = _HttpContextAccessor.HttpContext.Session.GetString("Id") + "_" + _HttpContextAccessor.HttpContext.Session.GetString("Stall")
                        + "_image" + index.ToString()+Path.GetExtension(image.FileName);
                }
                else if(index==0)
                {
                    uniqfilename = _HttpContextAccessor.HttpContext.Session.GetString("Id") + "_" + _HttpContextAccessor.HttpContext.Session.GetString("Stall")
                        + "_pdf" + Path.GetExtension(image.FileName);
                }
                else
                {
                    uniqfilename = _HttpContextAccessor.HttpContext.Session.GetString("Id") + "_" + _HttpContextAccessor.HttpContext.Session.GetString("Stall")
                        + "_logo" + index.ToString() + Path.GetExtension(image.FileName);
                }
                string filepath = Path.Combine(uploadfolder, uniqfilename);
                if(!Directory.Exists(uploadfolder))
                {
                    Directory.CreateDirectory(uploadfolder);
                }
                using (var fileStream = System.IO.File.Create(filepath))
                {
                    /*
                    if(System.IO.File.Exists(filepath))
                    {
                        System.IO.File.Delete(filepath);
                    }*/
                    image.CopyTo(fileStream);
                }

            }
            return uniqfilename;
        }
    }
}
