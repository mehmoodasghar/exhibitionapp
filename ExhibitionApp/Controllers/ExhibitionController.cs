﻿using ExhibitionApp.Data.DataModels;
using ExhibitionApp.Services.Abstract;
using ExhibitionApp.ViewModels;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ExhibitionApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ExhibitionController : ControllerBase
    {
        IStallService _stallService;
        IWebHostEnvironment _webHostEnvironment;
        IConfiguration _configuration;
        IContactCardService _cardService;
        ICompanyService _companyService;
        IFeedBackService _feedBackService;

        public ExhibitionController(IStallService stallService,IWebHostEnvironment webHostEnvironment,IConfiguration configuration
            ,IContactCardService contactCardService, ICompanyService companyService,IFeedBackService feedBackService )
        {
            _stallService = stallService;
            _webHostEnvironment = webHostEnvironment;
            _configuration = configuration;
            _cardService = contactCardService;
            _companyService = companyService;
            _feedBackService = feedBackService;
        }
        [HttpGet("getstalls")]
        public IActionResult GetStalls()
        {
            var s = _stallService.GetAllStalls();
            List<StallResponse> responses = new List<StallResponse>();
            foreach(Stall stall in s)
            {
                var resposne = new StallResponse();
                resposne.StallId = stall.Id;
                resposne.Logourl = PrepareDownLoadLink(stall.Logo);
                resposne.pdfUrl= PrepareDownLoadLink(stall.Pdf);
                resposne.Image1Url= PrepareDownLoadLink(stall.Image1);
                resposne.Image2Url= PrepareDownLoadLink(stall.Image2);
                resposne.Image3Url= PrepareDownLoadLink(stall.Image3);
                resposne.Image4Url= PrepareDownLoadLink(stall.Image4);
                resposne.YoutubeLink= stall.YoutubeLink;
                responses.Add(resposne);
            }
            return Ok(new { stalls = responses });
        }

        private string PrepareDownLoadLink(string file)
        {
            if (file != null)
            {
                string uploadfolder = Path.Combine(_webHostEnvironment.WebRootPath, "uploads");
                string completepath = Path.Combine(uploadfolder, file);
                var request = HttpContext.Request;
                var host = request.Host;
                completepath = "https://" + host.Value + "/Exhibition/GetFile?path=" + completepath;
                return completepath;
            }
            return null;
            
        }

        [HttpGet("GetFile")]
        public IActionResult DownloadFile(string path)
        {

            string contenttype;//=path.Substring(path.IndexOf('.'));
           // contenttype = contenttype.Substring(1);
            var provider = new FileExtensionContentTypeProvider();
            if(!provider.TryGetContentType(path,out contenttype))
            {
                contenttype = "application/octet-stream";
            }
            Byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, contenttype);
        }
        [HttpPost("submitfeedback")]
        public IActionResult SubmitFeedback([FromBody]FeedbackModel model)
        {
            try
            {
                var company = _companyService.GetById(model.CompanyId);
                FeedBack feedBack = new FeedBack();
                feedBack.CompanyId = model.CompanyId;
                feedBack.CreatedOn = DateTime.Now;
                feedBack.UpdatedOn = DateTime.Now;
                feedBack.Email = model.Email;
                feedBack.Message = model.Message;
                feedBack.Name = model.Name;
                feedBack.StallNo = model.StallNo;
                _feedBackService.AddFeedBack(feedBack);
                var mailMessage = new MimeMessage();
                mailMessage.From.Add(new MailboxAddress("Exhibition Admin", _configuration["Smtp:Username"]));
                mailMessage.To.Add(new MailboxAddress("User", company.Email));
                mailMessage.Subject = "Feed Back";
                mailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = "Feed Back " +
                    $"Name: {model.Name} \n" +
                    $"Email: {model.Email}\n" +
                    $"Message : {model.Message}\n"
                };

                using (var smtpclient = new SmtpClient())
                {
                    smtpclient.Connect(_configuration["Smtp:Host"], Convert.ToInt32(_configuration["Smtp:Port"]), true);
                    smtpclient.Authenticate(_configuration["Smtp:Username"], _configuration["Smtp:Password"]);
                    smtpclient.Send(mailMessage);
                    smtpclient.Disconnect(true);
                }


                return Ok(new { Message = "Feedback submitted successfully" });
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("sharecontactcard")]
        public IActionResult ShareContact([FromBody] ContactCardModal card)
        {
            ContactCard contactCard = new ContactCard();
            contactCard.Address = card.Address;
            contactCard.CompanyId = card.CompanyId;
            contactCard.Contact = card.Contact;
            contactCard.CreatedOn = DateTime.Now;
            contactCard.Email = card.Email;
            contactCard.Name = card.FirstName + " " + card.LastName;
            contactCard.UpdatedOn = DateTime.Now;
            _cardService.AddCard(contactCard);
            return Ok(new { Message = "Message Submitted successfully" });
        }


        [HttpGet("GetDetails")]
        public IActionResult GetDetails(LoginModel model)
        {
            var company = _companyService.Login(model);
            if (company != null)
            {
                return Ok(company);
            }
            else
            {
                return BadRequest("Invalid UserName or Password");
            }
        }
        
    }
}
