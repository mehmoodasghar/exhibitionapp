﻿using ExhibitionApp.Services.Abstract;
using ExhibitionApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExhibitionApp.Data.DataModels;

namespace ExhibitionApp.Controllers
{
    public class UserController : Controller
    {
        private IUserService _userService;
        private ICompanyService _companyService;

        public UserController(IUserService userService, ICompanyService companyService)
        {
            _userService = userService;
            _companyService = companyService;
        }
        public IActionResult Index()
        {
            if (chklogin())
            {
                var companies = _companyService.GetAll();
                return View(companies);
            }
            return RedirectToAction("Index", "Login");
        }

        public ActionResult CreateCompany(string title, string address, string email, string contact,string contactperson, string password)
        {
            Company comp = new Company();
            comp.Address = address;
            comp.ContactNo = contact;
            comp.ContactPerson = contactperson;
            comp.CreatedOn = DateTime.Now;
            comp.IsActive = true;
            comp.PassWord = password;
            comp.Title = title;
            comp.UpdatedOn = DateTime.Now;
            comp.UserName = email.ToLower();
            comp.Email = email;
            try
            {
                var chk=_companyService.Add(comp);
                if(chk==null)
                {
                    return Json(new { Success = false });
                }
                return Json(new { Success = true });
            }
            catch
            {
                return Json(new { Success = false });
            }
        }

        public IActionResult DeleteCompany(string id)
        {
            try
            {
                _companyService.Delete(id);
                return Json(new { Success = true });
            }
            catch
            {
                return Json(new { Success = false });
            }

        }

        public IActionResult UpdateCompany(string id, string title, string address, string email, string contact, string contactperson)
        {
            Company comp = _companyService.GetById(id);
            comp.Address = address;
            comp.ContactNo = contact;
            comp.ContactPerson = contactperson;
            comp.Title = title;
            comp.UserName = email;
            comp.Email = email;
            try
            {
                _companyService.UpdateCompany(comp);
                return Json(new { Success = true });
            }
            catch
            {
                return Json(new { Success = false });
            }

        }

        public IActionResult GetCompany(string id)
        {
            var company = _companyService.GetById(id);
            return Json(new { company= company, success = true });
        }

        private bool chklogin()
        {
            var chk = HttpContext.Session.GetString("Role");
            if (chk == null||chk== "Company")
            {
                return false;
            }
            return true;
        }
    }
}
