﻿using ExhibitionApp.Services.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExhibitionApp.Controllers
{
    public class CompanyController : Controller
    {
        ICompanyService _companyService;
        IContactCardService _cardService;
        IFeedBackService _feedBackService;

        public CompanyController(ICompanyService companyService,IContactCardService cardService,IFeedBackService feedBackService)
        {
            _companyService = companyService;
            _cardService = cardService;
            _feedBackService = feedBackService;
        }
        public IActionResult Index()
        {
            if (chklogin())
            {
                return View();
            }
            return RedirectToAction("Index", "Login");
        }
        private bool chklogin()
        {
            var chk = HttpContext.Session.GetString("Role");
            if(chk == null||chk == "Admin")
            {
                return false;
            }
            return true;
        }

        public IActionResult ListCompanies()
        {
            var role = HttpContext.Session.GetString("Role");
            if(role!=null)
            {
                if(role =="Admin")
                {
                    var companies = _companyService.GetAll();
                    return View(companies);
                }
                
            }
            return RedirectToAction("Index", "Login");
        }
        public IActionResult Cards()
        {
            var id = HttpContext.Session.GetString("Id");
            if (id != null && HttpContext.Session.GetString("Role") == "Company")
            {
                var cards = _cardService.GetCardsByCompany(id);
                return View(cards);
            }
            return RedirectToAction("Index", "Login");
        }

        public IActionResult FeedBack()
        {
            var id = HttpContext.Session.GetString("Id");
            if (id != null && HttpContext.Session.GetString("Role") == "Company")
            {
                var feedBacks = _feedBackService.GetByCompanyId(id);
                return View(feedBacks);
            }
            return RedirectToAction("Index", "Login");
        }
    }
}
