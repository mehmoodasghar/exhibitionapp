﻿using ExhibitionApp.Services.Abstract;
using ExhibitionApp.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExhibitionApp.Controllers
{
    public class LoginController : Controller
    {
        IUserService _userService;
        ICompanyService _companyService;
        public LoginController(IUserService userService,ICompanyService companyService)
        {
            _userService = userService;
            _companyService = companyService;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(LoginModel login)
        {
            if (ModelState.IsValid)
            {
                var loggedUser = _userService.Login(login);
                if (loggedUser != null)
                {
                    HttpContext.Session.SetString("Id", loggedUser.Id);
                    HttpContext.Session.SetString("Name", loggedUser.Name);
                    HttpContext.Session.SetString("Role", "Admin");
                    return RedirectToAction("Index","User");
                }

                var loggedCompany = _companyService.Login(login);
                if(loggedCompany!=null)
                {
                    HttpContext.Session.SetString("Id", loggedCompany.Id);
                    HttpContext.Session.SetString("Name", loggedCompany.Title);
                    HttpContext.Session.SetString("Role", "Company");
                    return RedirectToAction("Index", "Stall");
                }
            }
            return View(login);
        }

        public IActionResult UpdatePassword(string oldpassword, string newpassword)
        {
            if(HttpContext.Session.GetString("Role")=="Company")
            {
                var id = HttpContext.Session.GetString("Id");
                var chk = _companyService.UpdatePassword(id, oldpassword, newpassword);
                if (chk)
                {
                    return Json(new { Success = true });
                }
            }
            else if(HttpContext.Session.GetString("Role") == "Admin")
            {
                var id = HttpContext.Session.GetString("Id");
                var chk = _userService.UpdatePassword(id, oldpassword, newpassword);
                if (chk)
                {
                    return Json(new { Success = true });
                }
            }

            return Json(new { Success = false });
        }
    }
}
