﻿using ExhibitionApp.Data.DataModels;
using ExhibitionApp.Services.Abstract;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ExhibitionApp.Seeding
{
    public class SeedingConfigService : IHostedService
    {
        // IUserService _userService;
        IServiceProvider _services;

        public SeedingConfigService(IServiceProvider serviceProvider)
        {
            //_userService = userService;
            _services = serviceProvider;
        }

        public void seedDatabasewithAdmin(IUserService userService)
        {
            var user = new User();
            user.CreatedOn = DateTime.Now;
            user.Email = "admin@admin.com";
            user.Name = "Adminstrator";
            user.Password = "admin";
            user.UserName = "admin@admin.com";
            user.UpdatedOn = DateTime.Now;
            user.Role = "Admin";
            userService.AddUser(user);
        }
        public void seedDatabaseWithStalls(IStallService stallService)
        {
            if (stallService.GetAllStalls().Count == 0)
            {
                stallService.Create();
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            using var scope = _services.CreateScope();
            using var usercontext = scope.ServiceProvider.GetRequiredService<IUserService>();
            seedDatabasewithAdmin(usercontext);
            using var stallcontext = scope.ServiceProvider.GetRequiredService<IStallService>();
            seedDatabaseWithStalls(stallcontext);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
