function sidebarCollapse() {
    $('.collapse-sidebar').click(function () {
		$('.sidebar').toggleClass('sidebar-expand sidebar-collapsed');
		$('.sidebar .collapse-sidebar i').toggleClass('fa-angle-double-left fa-angle-double-right');
        $('.sidebar .sidebar-content ul.items li ul.dropdown-slide').toggleClass('dropdown dropdown-slide');
	});
}

function toggleNavMenu() {
    $('.nav-menu').click(function () {
        $('.sidebar').toggleClass('sidebar-desktop sidebar-mobile');
	});
}

function toggleFilter() {
    $(".filter-toggle").click(function(){
        if($("details.filter-block").css('display') == 'none') {
            $("details.filter-block").css('display', 'flex');
            $(".filter-toggle i").removeClass("fa-chevron-down");
            $(".filter-toggle i").addClass("fa-chevron-up");
        } else if($("details.filter-block").css('display') == 'flex') {
            $("details.filter-block").css('display', 'none');
            $(".filter-toggle i").removeClass("fa-chevron-up");
            $(".filter-toggle i").addClass("fa-chevron-down");
        }
      });
}

//function chartConfig() {
//    var options = {
//		series: [50, 25, 20],
//		chart: {
//		  height: 180,
//		  type: "donut",
//		},
//		stroke: {
//		  width: 2
//		},
//		plotOptions: {
//		  pie: {
//			donut: {
//			  labels: {
//				show: true,
//				  value: {
//					show: true,
//					fontSize: '34px',
//					fontFamily: 'Muli, Helvetica, Arial, sans-serif',
//					fontWeight: 400,
//					color: undefined,
//					offsetY: 10,
//					formatter: function (val) {
//					  return val
//					}
//				},		  
//					total: {
//					showAlways: true,
//					show: true,
//					label: 'Total',
//					fontSize: '14px',
//					fontWeight: '200',
//					fontFamily: 'Muli, Helvetica, Arial, sans-serif',
//					}
//			  }
//			}
//		  }
//		},
//		legend: {
//		 	show: false
//		},
//		dataLabels: {
//			enabled: false
//	    },
//		labels: ["Assigned", "Unassigned", "Pending"],
//		colors: ['#E30613', '#f18289', '#fce6e7'],
//		fill: {
//			colors: ['#E30613', '#f18289', '#fce6e7']
//		},
//		states: {
//		  hover: {
//			filter: {
//			  type: "none"
//			}
//		  }
//		},
//		responsive: [
//		  {
//			breakpoint: 767,
//			options: {
//			  chart: {
//				offsetX: 0,
//			  },
//			  legend: {
//				position: "bottom"
//			  },
//			  dataLabels: {
//				enabled: true
//			  },
//			  plotOptions: {
//				pie: {
//					donut: {
//					  size: '40%',

//					  labels: {
//						show: true,
//						  value: {
//							show: true,
//							fontSize: '22px',
//							offsetY: -5,
//						},		  
//							total: {
//							fontSize: '12px',
//							}
//					  }
//					}
//				  }
//			  }
//			}
//		  },
//		  {
//			breakpoint: 900,
//			options: {
//			  chart: {
//				height: 150,
//				offsetX: 0,
//			  },
//			  dataLabels: {
//				enabled: true
//			  },
//			  plotOptions: {
//				pie: {
//					donut: {
//					  size: '45%',

//					  labels: {
//						show: true,
//						  value: {
//							fontSize: '22px',
//							offsetY: -5
//						},		  
//						total: {
//							fontSize: '12px',
//							}
//					  }
//					}
//				  }
//			  }
//			}
//		  },
//		]
//	  };

//	  var chart1 = new ApexCharts(document.querySelector("#chart1"), options);
//	  chart1.render();

//	  var chart2 = new ApexCharts(document.querySelector("#chart2"), options);
//	  chart2.render();

//	//   var chart3 = new ApexCharts(document.querySelector("#chart3"), options);
//	//   chart3.render();
//}

function dateRange() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        opens: 'left',
        alwaysShowCalendars: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
}

function enableTooltip() {
	$("body").tooltip({ selector: '[data-toggle=tooltip]' });
}

function multiSelect() {
	$("#multiSelect").multiselect({
		includeSelectAllOption: true
	});
}
