$(document).ready(function () {
    sidebarCollapse();
    toggleNavMenu();
    toggleFilter();
    chartConfig();
    dateRange();
    enableTooltip();
    multiSelect();
});
